package me.kodysimpson;

//To create an annotation, simple use the interface keyword preceded by an @
//Annotations cannot by extended by you, but extend Annotation by default
public @interface EmptyAnnotation {

    //annotations can be empty

}
