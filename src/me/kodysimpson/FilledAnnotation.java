package me.kodysimpson;

//I called it Filled just because it has values
public @interface FilledAnnotation {

    String name();
    int value() default 25; //You can add default values to it so that they arent required

}
