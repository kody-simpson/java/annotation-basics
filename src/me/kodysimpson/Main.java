package me.kodysimpson;

//Annotations can be placed on: Classes, methods, fields, parameters, enumerators
//in jdk 8, Annotations can be placed in more special places like object instantiations,
//we will take a look at that later.
@EmptyAnnotation
public class Main {

    @EmptyAnnotation
    private static final int THING = 100;

    @EmptyAnnotation
    public static void main(String[] args) {
	// write your code here
    }

    @FilledAnnotation(name = "Bob", value = 123)
    public static void buildWall(){

        //in the future we can do cool things like access the
        //annotation values using reflection.

    }

}
